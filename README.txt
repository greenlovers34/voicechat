-------------------------------------
- Module : Sound API Java v.1.0
- Author : Daniel Jonathan 
- Contains : 	-> VoiceRecorder unit
		-> VoicePlayer unit
		-> VoiceSender unit
		-> VoiceReceiver unit
		-> Test drivers
-------------------------------------

This is a module for manipulating audio in Java, 
You may use this code freely, but please credit author
if you wish to copy-paste :)
  
They are designed to be reused and handpicked
To test component, do build all testers in test package 

Note : These modules are not integrated.
I have given pieces of the puzzle. 
It's up to your creativity to integrate all of them.
