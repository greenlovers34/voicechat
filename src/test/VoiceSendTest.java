package test;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JFrame;

import voice.VoiceSender;

/**
 * Driver class that test VoiceSender
 * @author Daniel Jonathan
 *
 */
public class VoiceSendTest {
	public static void main(String[] args) {
		VoiceSender sender = new VoiceSender();
		sender.setSize(400,400);
		sender.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		sender.setVisible(true);
		
		try {
			VoiceSender.chooseFile(sender);
			VoiceSender.sendVoiceChat("localhost");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException interrup) {
			interrup.printStackTrace();
		}
	}
	
}
