package test;
import voice.*;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Dimension;

import javax.sound.sampled.LineUnavailableException;

import java.io.IOException;

/**
 * Driver to test voice recording
 * voice player
 * @author Daniel Jonathan
 *
 */
public class GUITest extends JFrame implements ActionListener {

	private JButton recordButton;
	private JButton sendMsgBtn;
	
	private JTextArea inputArea;
	private JTextPane outputArea;
	
	private VoiceRecorder recorder;

	
	private JPanel chatPanel; 
	private JPanel footerPanel;
	private JPanel inputPanel;
	private JPanel sendMsgPanel;
	
	private JLabel statusLabel;
	
	private JScrollPane scrollPane;

	public GUITest() {
		super("Voice Record driver");
		
		setLayout(new BorderLayout());
		
		// initialize outputArea
		outputArea = new JTextPane();
		
		// initialize scrollPane with area as container
		scrollPane = new JScrollPane(outputArea);
		scrollPane.setPreferredSize(new Dimension(500, 300));
		
		// initialize send button
		sendMsgBtn = new JButton("Send");
		sendMsgBtn.addActionListener(this);
		
		// initialize record button
		recordButton = new JButton("Record");
		recordButton.addActionListener(this);
		
		// initialize inputArea
		inputArea = new JTextArea(5, 20);
		inputArea.setLineWrap(true);
		
		// initialize status label
		statusLabel = new JLabel("", SwingConstants.RIGHT);
		
		initializePanel();
		
		// initialize recorder
		recorder = new VoiceRecorder();
		
		add(chatPanel, BorderLayout.NORTH);
		add(inputPanel, BorderLayout.CENTER);
		add(footerPanel, BorderLayout.SOUTH);
	}
	
	
	private void initializePanel() {
		
		// initialize chat panel
		chatPanel = new JPanel();
		chatPanel.setLayout(new BorderLayout());
		chatPanel.add(scrollPane, BorderLayout.CENTER);
		
		// initialize sendMsgPanel
		sendMsgPanel = new JPanel();
		sendMsgPanel.setLayout(new BorderLayout());
		sendMsgPanel.add(sendMsgBtn, BorderLayout.NORTH);
		sendMsgPanel.add(recordButton, BorderLayout.CENTER);
		
		// initialize input panel
		inputPanel = new JPanel();
		inputPanel.setLayout(new BorderLayout());
		inputPanel.add(inputArea, BorderLayout.CENTER);
		inputPanel.add(sendMsgPanel, BorderLayout.EAST);
		
		// initialize footer panel
		footerPanel = new JPanel();
		footerPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
		footerPanel.add(statusLabel);
	}
		
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == recordButton) {
			handleRecordButton();
		} 
		else if (e.getSource() == sendMsgBtn) {
			appendToPane(outputArea, inputArea.getText() + "\n", Color.BLUE);
			inputArea.setText("");
			refreshGUI();
		}
	}
	
	/**
	 * Record Button handler method
	 */
	private void handleRecordButton() {
		RecordHandler recorderHandler = new RecordHandler(recorder);
		recorderHandler.start();
		waitFlagSettingBeforeUpdate();
		setRecordButtonState(recorder.isRecording());
		extractRecorderFile(recorderHandler.isSuccessful());
	}
	
	/**
	 * append
	 * @param tp :
	 * @param msg
	 * @param c
	 * @author nIcE cOw
	 */
    private void appendToPane(JTextPane tp, String msg, Color c)
    {
        StyleContext sc = StyleContext.getDefaultStyleContext();
        AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);

        aset = sc.addAttribute(aset, StyleConstants.FontFamily, "Lucida Console");
        aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);

        int len = tp.getDocument().getLength();
        tp.setCaretPosition(len);
        tp.setCharacterAttributes(aset, false);
        tp.replaceSelection(msg);
    }
	
	
	/**
	 * Sleep the caller thread, wait for record handler
	 * to set whether the microphone is recording or not
	 * so other component can used it
	 */
	private void waitFlagSettingBeforeUpdate() {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/**
	 * extract the voice wav file and append it to JTextArea if it's successful
	 * 
	 * @param isRecording
	 */
	private void extractRecorderFile(boolean successful) {
		if (successful) {
			// append to inputArea to be sent later
			inputArea.append(recorder.getRecordName());
			refreshGUI();
		}	
	}
	
	private void setRecordButtonState(boolean isRecording) {
		statusLabel.setText(isRecording ? "Recording.." : "Stopped");
		recordButton.setText(isRecording ? "Stop" : "Record");
		recordButton.setPreferredSize(recordButton.getMinimumSize());
		
		refreshGUI();
	}
	
	private void refreshGUI() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				repaint();
			}
		});
	}
	
	public static void main(String[] args) {
		
		GUITest gui = new GUITest();
		gui.setSize(500,420);
		gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		gui.setVisible(true);
		
	}

}
