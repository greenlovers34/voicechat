package voice;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * Class that listens to handshake from
 * Sender, determines whether receiving end
 * must be done by batch, or send at once,
 * and finally retrieves the batches
 * 
 * @author Daniel Jonathan
 *
 */
public class VoiceReceiver {
	
	static int voiceSize;
	static int batchSize;
	static int actualBatch;
	
	/**
	 * Listens to sender, receive handshake containing 
	 * the size of the file, then determines whether it 
	 * must be received by batch or at once
	 *
	 * @throws SocketException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static void receive() throws SocketException, IOException, ClassNotFoundException {
		
		switch(listenToHandshake()) {
			case AT_ONCE:
				receiveAtOnce();
				break;
			case BATCH:
				receiveByBatch();
				break;
		}
	}
	
	/**
	 * Listen to handshakes, get the size of the audio file
	 * determines the amount of batches to receive
	 */
	private static SendType listenToHandshake() throws IOException, ClassNotFoundException {
		
		ServerSocket listener = new ServerSocket(8888);
		System.out.println("Listening...");
		Socket socket = listener.accept();
		
		ObjectInputStream sizeReceiver = new ObjectInputStream(socket.getInputStream());
		int packetSize = ((Integer) sizeReceiver.readObject()).intValue();
		System.out.println("Packet size is : " + packetSize); 
		System.out.println("Object read");
		
		voiceSize = packetSize;
		batchSize = SendType.checkAmountOfBatches(packetSize);
		
		System.out.println("Batch size " + batchSize);
		System.out.println("Voice size " + voiceSize);
		
		socket.close();
		listener.close();
		
		return SendType.checkSendType(packetSize);
	}


	private static void receiveAtOnce() throws IOException {

		DatagramSocket receiver = new DatagramSocket(8888);
		DatagramPacket voicePacket = new DatagramPacket(new byte[voiceSize], voiceSize);
		
		receiver.receive(voicePacket);
		System.out.println("Voice packet received, length " + voicePacket.getLength());		
		
		receiver.close();
		
		// write the file to current dir
		FileOutputStream stream = new FileOutputStream("test.wav");
		stream.write(voicePacket.getData());
		stream.close();
		
		
		// play the file
		VoicePlayer player = new VoicePlayer();
		player.playSound("test.wav");
	}
	
	/**
	 * receive split audio file in the form of array of batches,
	 * assemble these batches into one single array bytes,
	 * and construct the array into single audio files.
	 *
	 * @throws SocketException
	 * @throws IOException
	 */
	private static void receiveByBatch() throws SocketException, IOException {
		
		actualBatch = 0;
		
		// create byte batches
		byte[][] voicePackets = new byte[batchSize][SendType.MAX_BATCH_SIZE];
		// full Size voice byte
		byte[] voicePacket = new byte[voiceSize];
		
		DatagramSocket socket = new DatagramSocket(8888);
		DatagramPacket packet = new DatagramPacket(voicePackets[0], SendType.MAX_BATCH_SIZE);
		
		// receive the packet by batches
		for (int batch = 0; batch < batchSize; batch++) {
			packet.setData(voicePackets[batch]);
			socket.receive(packet);
			actualBatch++;
			System.out.printf("Received batch %d/%d", actualBatch, batchSize);
		}
		
		socket.close();
		
		// fill the voicePacket byte from the voicePackets
		// reconstruct voicePacket
		for (int batch = 0; batch < batchSize; batch++) {
			
			for (int block = 0; block < SendType.MAX_BATCH_SIZE; block++) {
				
				if (block + (batch * SendType.MAX_BATCH_SIZE) == voiceSize)
					break;
				
				voicePacket[block + (batch * SendType.MAX_BATCH_SIZE)] = voicePackets[batch][block];
			}
		}
		
		
		// create the mp3 from the voicePacket
		FileOutputStream outStream = new FileOutputStream("test.wav");
		
		outStream.write(voicePacket);		
		outStream.close();
		
		// play the file
		VoicePlayer player = new VoicePlayer();
		player.playSound("test.wav");
	}
}
