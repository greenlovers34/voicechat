package voice;
import java.io.ObjectOutputStream;
import java.io.IOException;
import java.io.FileInputStream;
import java.math.BigInteger;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.rmi.UnknownHostException;

import javax.swing.JFrame;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.Component;
import java.io.File;
import java.io.FileNotFoundException;

public class VoiceSender extends JFrame {
	
	static File voiceFile = null;
	
	/**
	 * Send voice chat to a specific address,
	 * The caller must first choose file first,
	 * using chooseFile method 
	 * otherwise, a FileNotFoundException might be thrown
	 * 
	 * @param address
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static void sendVoiceChat(String address) throws FileNotFoundException, IOException, InterruptedException {	
		
		switch(performHandshake(address)) {
			case AT_ONCE:
				beginSendAtOnce(address);
				return;
			case BATCH:
				beginSendByBatch(address);
				return;
		}
	}
	
	/**
	 * Perform handshake, in this case
	 * the sender sends the size of the packet to the receiver and
	 * determines whether the delivery should be sent at once,
	 * or batch by batch
	 * 
	 * @param address : address to be sent to 
	 * @return SendType constant
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	private static SendType performHandshake(final String address) throws UnknownHostException, IOException {

		int length = (int) voiceFile.length();
		
		System.out.println("length is: " + length);
		
		Socket sender = new Socket(address, 8888);
		ObjectOutputStream outputStream = new ObjectOutputStream(sender.getOutputStream());
		outputStream.writeObject(new Integer(length));
		
		sender.close();
		
		return SendType.checkSendType(length);
	}
	

	/**
	 * Sends all voice chat packets at once,
	 * using UDP protocol.
	 * @param address
	 * @throws IOException
	 * @throws InterruptedException
	 * @author Daniel Jonathan
	 */
	private static void beginSendAtOnce(final String address) throws IOException, InterruptedException {
		
		// Get the file input stream and read 
		// it to byte array
		FileInputStream inStream = new FileInputStream(voiceFile);
		int length = (int) voiceFile.length();
		
		byte[] voiceData = new byte[length];
		inStream.read(voiceData, 0, length);
		inStream.close();
		
		// begin crafting the voice packet from byte array
		DatagramPacket voicePacket = new DatagramPacket(voiceData, voiceData.length, InetAddress.getByName(address), 8888);
		DatagramSocket socket = new DatagramSocket();
		
		// ensure the receiver starts calling receive method first
		Thread.sleep(10);
		// sent the voice
		socket.send(voicePacket);
		System.out.println("Voice packet sent");
		
		socket.close();
	}
	
	/**
	 * Splits the entire byte array into batches,
	 * each batches contains block of bytes with max size of 4KB
	 * sends all of them, one by one.
	 * @param address
	 * @throws InterruptedException
	 * @throws IOException
	 * @author Daniel Jonathan
	 */
	private static void beginSendByBatch(final String address) throws InterruptedException, IOException {
		
		int length = (int) voiceFile.length();
		int batches = SendType.checkAmountOfBatches(length);
		
		FileInputStream inStream = new FileInputStream(voiceFile);
		byte[][] voicePackets = new byte[batches][SendType.MAX_BATCH_SIZE];
		
		byte[] voicePacket = new byte[length];
		
		inStream.read(voicePacket, 0, length);
		inStream.close();
		// separating packets
		for (int batch = 0; batch < batches; batch++) {
			
			for (int block = 0; block < SendType.MAX_BATCH_SIZE; block++) {
				
				if (block + (batch * SendType.MAX_BATCH_SIZE) == voicePacket.length)
					break;
				
				voicePackets[batch][block] = voicePacket[block + (batch * SendType.MAX_BATCH_SIZE)];
			}
		}
		
		DatagramPacket packet = new DatagramPacket(voicePacket, length, InetAddress.getByName(address), 8888);
		DatagramSocket socket = new DatagramSocket();
		// send packets by batch
		for (int batch = 0; batch < batches; batch++) {
			Thread.sleep(80);
			packet.setData(voicePackets[batch]);
			packet.setLength(SendType.MAX_BATCH_SIZE);
			socket.send(packet);
		}
			
		socket.close();
		
	}
	
	public static byte[] getSizeInByteArray(final int size) {
		
		BigInteger integer = BigInteger.valueOf(size);
		return integer.toByteArray();
	}
	
	public static void chooseFile(Component parent) throws IOException, FileNotFoundException, InterruptedException {
			
			JFileChooser fileChooser = new JFileChooser();
			FileNameExtensionFilter filter = new FileNameExtensionFilter("WAV File", "wav");
			fileChooser.setFileFilter(filter);
			
			switch(fileChooser.showOpenDialog(parent)) {
			
				case (JFileChooser.APPROVE_OPTION): 
					voiceFile = fileChooser.getSelectedFile();
					break;
				case (JFileChooser.CANCEL_OPTION):
					return;
			
			}
	}
	
	public static String getvoiceFile() {
		return voiceFile.getName();
	}
}