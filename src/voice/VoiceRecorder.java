package voice;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

/**
 * Class that records, and store the 
 * wav files in the current directory
 * these methods are blocking method,
 * please use this object with 
 * RecordHandler thread
 * 
 * @author Daniel Jonathan
 *
 */

public class VoiceRecorder {

	private File recordFile;
	private String recordName;
	private boolean recording = false;
	private AudioFormat soundFormat;
	private AudioFileFormat.Type extension;
	private TargetDataLine micLine;

	
	public VoiceRecorder() {
		try {
			setupAudioFormat();
		}
		catch (LineUnavailableException ex) {
			System.err.println("Setup error..");
			ex.printStackTrace();
		}
	}
	
	private void setupAudioFormat() throws LineUnavailableException {
		extension = AudioFileFormat.Type.WAVE;
	    soundFormat = new AudioFormat(16000, 8, 2, true, true);
	    
	    DataLine.Info outputInfo = new DataLine.Info(TargetDataLine.class, soundFormat);
	    
	    // get the microphone line
		micLine = (TargetDataLine) AudioSystem.getLine(outputInfo);
	}
	
	private void constructRecordFile() {
		
		Calendar now = Calendar.getInstance();
		String fileName = String.format("rec%s%s%s%s%s%s.wav", 
				now.get(Calendar.DAY_OF_MONTH), now.get(Calendar.MONTH + 1), 
				now.get(Calendar.YEAR), now.get(Calendar.HOUR_OF_DAY), 
				now.get(Calendar.MINUTE), now.get(Calendar.SECOND));
		
		recordName = fileName;
		recordFile = new File(recordName);
		
	}
	
	public void startRecording() throws IOException, LineUnavailableException {
	
		this.recording = true;
		constructRecordFile();
		
		// open the microphone line using format
		micLine.open(soundFormat);

		// start receiving input 
		micLine.start();	
		AudioInputStream audioIn = new AudioInputStream(micLine);
		
		// write voice input in file
		AudioSystem.write(audioIn, extension, recordFile);
	}
	
	public void stopRecording() {
	
		this.recording = false;
		
		micLine.stop();
		micLine.close();
	}
	
	public boolean isRecording() {
		return recording;
	}
	
	public File getRecordFile() {
		return recordFile;
	}

	public String getRecordName() {
		return recordFile.getName();
	}
	
}
